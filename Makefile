include Makefile.std
TOPTARGETS := all clean

EXEC=proj

.PHONY: test

all: $(OBJECTS) $(OBJLIB) 
	$(CXX) $(OBJECTS)  $(OBJLIB) $(LDFLAGS) -o $(EXEC)

$(EXEC): $(OBJLIB)


test:
	-./proj
	-./proj --help
	-rm -rf ./test
	mkdir test
	cd test; ../proj --define project=newproj --define license=MIT --define copyright=TOPR ./
