#include <iostream>
#include "options.hpp"
using namespace std ;
int main(int argc, char **argv) {
    Options options("template", "command line utility template", "V0.0") ;
    if (options.Analyze(argc, argv)) {
        cout << "Continuing" << endl ;
    } else {
        exit(EXIT_FAILURE) ;
    }
}