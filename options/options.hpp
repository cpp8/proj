#ifndef _OPTIONS_HPP_
#define _OPTIONS_HPP_
#include <string>
#include <vector>

class Options {
public:
    Options(std::string _name, std::string _desc, std::string _version) ;
    bool Analyze(int _argc, char **_argv) ;
    void ShowVersionDetail() const ;
    void Show() const ;
    bool Verbose ;
    std::vector< std::string > Macros ;
    std::string repository ;
    std::string destination ;
private:
    std::string name ;
    std::string description ;
    std::string version ;
    int argc ;
    char **argv ;
    Options() ;
} ;

#endif