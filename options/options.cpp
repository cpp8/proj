#include <iostream>
#include <boost/program_options.hpp>
using namespace boost ;
namespace po = boost::program_options ;
#include <algorithm>
#include <iterator>
#include <string>
#include <vector>

#include "options.hpp"
#include "revisions.h"

using namespace std ;
using namespace po ;

const string default_repository = "git@gitlab.com:cpp8/cli.git" ;

template<class T>
ostream& operator<<(ostream& os, const vector<T>& v)
{
    copy(v.begin(), v.end(), ostream_iterator<T>(os, " "));
    return os;
}

Options::Options(std::string _name, std::string _desc, std::string _version) 
: Verbose(false) , name(_name) , description(_desc) , version(_version) 
{

}

bool Options::Analyze(int _argc, char **_argv) {

    argc = _argc ;
    argv = _argv ;

    po::options_description generic( name + " - " + description + " - " + version + "\n" +
                                     "usage : [options] <macro definitions>... <destination> \nOptions " );
    generic.add_options()
        ("verbose,v", "set verbose")
        ("version,V" , "show version")
        ("help,h", "produce help message")    
        ("define,D" , value< vector < std::string > >() , "one or more macro definitions")
        ("repository,r" , value< string > () -> default_value(default_repository) , "repository to clone")
        ;
 
    po::options_description hidden ;
    hidden.add_options()
            ("destination" , value<string>() , "destination directory")
            ;

    po::options_description cmdline_options;
    cmdline_options.add(generic).add(hidden) ;   

    po::variables_map vm;

    po::positional_options_description pos ;
    pos.add("destination",1);

    if (argc < 2) {
        cerr << generic << endl ;
        return false ;
    }

    try {
        po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(pos).run(), vm);
    } 
    catch(std::exception& e) {
        cerr << e.what() << endl ;
        return false;
    } 
    po::notify(vm);    

    if (vm.count("help")) {
        cerr << generic << endl ;
        return false ;
    }

    if (vm.count("verbose")) {
        Verbose = true ;
    }

    if (vm.count("version")) {
        ShowVersionDetail() ;
        return false ;
    }

    repository = vm["repository"].as< string >() ;

    if (vm.count("destination"))
    {
        destination = vm["destination"].as< string >() ;
    } else {
        cerr << "destination directory is required" << endl ;
        cerr << generic << endl ;
        return false ;
    }

    if (vm.count("define"))
    {
        auto macros = vm["define"].as< vector < string > >() ;
        if (macros.size() < 1) {
            if (Verbose) cerr << "No macros defined" << endl ;
        } else {
            Macros = macros ;
        }
    }
    if (Verbose) Show() ;
    return true ;
}

void Options::ShowVersionDetail() const {
    cout << name << " - " << description << " - " << __DATE__ << " " << __TIME__ << endl ;
    cout << "Major: " << VERSION_MAJOR << " Minor : " << VERSION_MINOR << " Build: " << VERSION_BUILD << endl ;
    cout << "Version Recorded: " << BUILD_TIME << endl ;
    cout << "Repo URL: " << REPO_URL << " Branch: " << BRANCH_NAME << " Commit Id: " << SHORT_COMMIT_ID << " Tags: " << ASSIGNED_TAGS << endl ;
}

void Options::Show() const {
    cout << "Macros are: " << Macros << endl ;
    cout << "Template repository to clone " << repository << endl ;
    cout << "Destination directory " << destination << endl ;
}

