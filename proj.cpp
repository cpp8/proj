#include <iostream>
#include "options/options.hpp"
#include "config/config.hpp"
#include "clone/clone.hpp"

using namespace std ;

int main(int argc, char **argv) {
    Options options("proj", "Project clone", "V0.0") ;
    if (options.Analyze(argc, argv)) {
        cout << "Continuing" << endl ;
    } else {
        exit(EXIT_FAILURE) ;
    }

    bool bstat ;
    Clone clone ;
    bstat = clone.Download(options.repository,options.destination) ;
    if (!bstat) {
        cerr << "Download failed. Quitting..." << endl ;
        exit(EXIT_FAILURE) ;
    }

    bstat = clone.Configure(options.Macros) ;
    if (!bstat) {
        cerr << "Configure failed. Quitting..." << endl ;
        exit(EXIT_FAILURE) ;
    }

    bstat = clone.Transform() ;
    if (!bstat) {
        cerr << "Transform failed. Quitting..." << endl ;
        exit(EXIT_FAILURE) ;
    }
    exit(EXIT_SUCCESS) ;

}