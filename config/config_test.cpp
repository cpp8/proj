#include <iostream>
#include "config.hpp"

using namespace std ;

int main(int argc, char **argv) {
    Config config ;
    config.Load("proj.cfg");
    config.Show();
    cout << "Test 2 " << endl ;
    config.Set("option1" , "20");
    config.Set("option2" , "Variable++");
    config.Show();
    cout << "Test 3" << endl ;
    config.Set("option1=100") ;
    config.Set("option2=Constant") ;
    config.Show() ;
    cout << "option1 - value is " << config.Get("option1") << endl ;
    cout << "option2 - value is " << config.Get("option2") << endl ;
    cout << "Random - value is " << config.Get("Random") << endl ;
}