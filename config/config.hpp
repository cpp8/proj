#ifndef _CONFIG_HPP_
#define _CONFIG_HPP_
#include <string>
#include <vector>
#include <unordered_map>
#include <glib.h>

class Macro {
public:
   std::string name ;
   std::string defval ;
   std::string help ;
   void Show() const ;
} ;
typedef std::unordered_map< std::string , Macro > Macros ;

class Config{
public:
   Config() ;
   bool Load(std::string _cfgfile) ;
   void Set(std::string _macro, std::string _value);
   void Set(std::string _macrodef) ;
   void Set(std::vector< std::string > _macrodefs) ;
   std::string Get(std::string _mac) ;
   void Show() ;
   bool Verbose ;
private:
   Macros macros ;
} ;

#endif