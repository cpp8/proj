#include <iostream>
#include <vector>
#include <string>
#include <assert.h>
#include <glib.h>
#include "config.hpp"
using namespace std ;

void Macro::Show() const {
    cout << "Macro " << name << endl ;
    cout << "Default: " << defval << endl ;
    cout << "Help: " << help << endl ;
}

Config::Config() 
: Verbose(false)
{

}

bool Config::Load(std::string _filename) {
    GKeyFile *cfgfile ;
    cfgfile = g_key_file_new();
    GError *error = NULL ;
    gboolean status ;
    status = g_key_file_load_from_file (cfgfile,
                                        _filename.c_str(),
                                        G_KEY_FILE_NONE,
                                        &error );
    if (status != TRUE) {
        cerr << "Failed to load config from " << _filename << endl ;
        cerr << error->message << endl ;
        g_key_file_free(cfgfile) ;
        return false ;
    }

    gchar **groups ;
    gsize length ;
    groups =  g_key_file_get_groups ( cfgfile ,
                                     &length ) ;
    if (length <= 1) {
        cerr << "No optional macros" << endl ;
        if (groups != NULL) {
            g_strfreev(groups) ;
        }
    }
    gchar *temp ;
    for (int i=0; i<length; i++) {
        if (strcmp(groups[i],"repository") != 0) {
            Macro macro ;
            macro.name = groups[i] ;
            temp = g_key_file_get_value (cfgfile,
                               groups[i] ,
                                "default" ,
                                NULL );
            if (temp != NULL) {
                macro.defval = temp ;
            }
 
            temp = g_key_file_get_value (cfgfile,
                                groups[i] ,
                                "help" ,
                                NULL );
            if (temp != NULL) {
                macro.help = temp ;
            }           

            macros.insert_or_assign( macro.name , macro ) ;  
        }
    }
    g_key_file_free(cfgfile) ;
    return true ;
}
void Config::Set(std::string _macro, std::string _value) {
    if (Verbose) cout << "Macro " << _macro << " value " << _value << " defined" << endl ;
    try {
        auto &old = macros.at(_macro) ;
        old.defval = _value ;
    }
    catch (const std::out_of_range e) {
        Macro macro ;
        macro.name = _macro ;
        macro.defval = _value ;
        macro.help = "new definition. not in the original list" ;
        macros.insert_or_assign(macro.name,macro);
    }
}
void Config::Set(std::string _macrodef) {
    auto start = 0U; 
    auto end = _macrodef.find("=");
    if (end == std::string::npos) {
        Set( _macrodef , "true") ;
        return ;
    }
    Set( _macrodef.substr(0U, end) , _macrodef.substr(end+1, std::string::npos) );
}

void Config::Set(std::vector< std::string > _macrodefs) {
    for (std::vector<std::string>::iterator str=_macrodefs.begin(); str != _macrodefs.end(); str++) {
        if (Verbose) cout << "Set " << *str << endl ;
        Set( *str ) ;
    }
}
std::string Config::Get(std::string _mac) {
    try {
        auto &old = macros.at(_mac) ;
        return old.defval ;
    }
    catch (const std::out_of_range e) {
        return "" ;
    }
}
void Config::Show() {
    for( const auto& m : macros ) {
        std::cout << "Key:[" << m.first ;
        m.second.Show() ;
    }
}