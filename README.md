# proj

## Usage
```
 ./proj
proj - Project clone - V0.0
usage : [options] <macro definitions>... <destination> 
Options :
  -v [ --verbose ]                      set verbose
  -V [ --version ]                      show version
  -h [ --help ]                         produce help message
  -D [ --define ] arg                   one or more macro definitions
  -r [ --repository ] arg (=git@gitlab.com:cpp8/cli.git)
                                        repository to clone
```

## Create a simple cli utility
```
mkdir test; cd test; ../proj --define project=newproj --define license=MIT --define copyright=TOPR ./
Continuing
Cloning into '/home/pi/Prj/proj/test/.'...


Set project=newproj
Macro project value newproj defined
Set license=MIT
Macro license value MIT defined
Set copyright=TOPR
Macro copyright value TOPR defined
// Copyright 2020 TOPR                            proj:subs srini TOPR
// License: MIT                                proj:subs unknown MIT
```

### the macro processed file
```
// proj:rename newproj.cpp
// Copyright 2020 TOPR                            proj:subs srini TOPR
// License: MIT                                proj:subs unknown MIT

#include <iostream>
#include "options/options.hpp"

using namespace std ;

int main(int argc, char **argv) {
    Options options("template", "command line utility template", "V0.0") ; // proj:substitute template newproj
    if (options.Analyze(argc, argv)) {
        cout << "Continuing" << endl ;
    } else {
        exit(EXIT_FAILURE) ;
    }
}
```