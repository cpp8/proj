#ifndef _CLONE_HPP_
#define _CLONE_HPP_
#include <string>
#include <vector>
#include "../config/config.hpp"

typedef enum TransformStatus {
   SUCCESS ,
   NOCHANGE , 
   UNDEFINED_MACRO ,
   FILENAMECHANGE
} TransformStatusType ;

class Clone {
public:
    Clone() ;
    bool Download(std::string _repo, std::string _dest) ;
    bool Configure(std::vector< std::string > _overrides) ;
    bool Transform() ;
    bool Transform(std::string _filename) ;
    std::string Expand(std::string _line) ;
    TransformStatus TransformLine(std::string _line, std::string &_oline) ;

    bool Verbose ;
private:
    std::string destination ;
    Config config ;
} ;
#endif