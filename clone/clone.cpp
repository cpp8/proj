#include <iostream>
#include <filesystem>
#include <fstream>
#include <regex>
#include <algorithm>
#include <boost/algorithm/string/replace.hpp>

#include <glib.h>
#include "clone.hpp"

using namespace std ;
namespace fs = std::filesystem ;

const std::string directive("proj:") ;

const std::string command ("git clone ") ;
const std::string configfile( "Clone.cfg") ;
std::regex usemacro ("\\$\\(([a-zA-Z].*)\\)") ;

std::regex renamecmd ("rename\\s+(.*)\\s*") ;
std::regex subscmd ("subs ([a-zA-Z0-9_]*) ([a-zA-Z0-9_]*)") ;

Clone::Clone() 
: Verbose(false)
{

}

bool Clone::Download(std::string _repo, std::string _dest) {
    fs::path p(_dest);
    fs::directory_entry de(p);
    if (de.exists()) {
        if (de.is_directory()) { 
            destination = fs::absolute(_dest) ;
            if (Verbose) cout << "Directory " << _dest << " already exists. full name " << destination << endl ;
        } else {
            cerr << "File " << _dest << " exists but is not a directory" << endl ;
            return false ;
        }
    } else {
        std::error_code ec ;
        if (fs::create_directory(p,ec)) {
            destination = fs::absolute(_dest) ;
            if (Verbose) cout << "Directory " << _dest << " created as " << destination << endl ;
        } else {
            cerr << "Unable to create " << _dest << endl ;
        }
    }

    string fullcmd = command + " " + _repo + " " + destination ;
    if (Verbose) cout << "Executing " << fullcmd << endl ;
    GError *error =NULL;
    gchar *stderr =NULL;
    gchar *stdout =NULL;
    gboolean status = false ;
    gint exit_status = 0 ;
    status = g_spawn_command_line_sync( fullcmd.c_str() , &stderr, &stdout, &exit_status, &error ) ;
    if (status) {
        cout << stdout << endl ;
        cout << stderr << endl ;
    } else {
        cerr << "spawn failed" << endl ;
        return false ;
    }
  
    return true ;
}

bool Clone::Configure(std::vector< std::string > _overrides) {
    fs::path cfgpath (destination) ;
    cfgpath /= configfile ;
    fs::directory_entry de(cfgpath) ;
    if (de.exists()) {
        if (Verbose) cout << "Config file is " << cfgpath.c_str() << " exists " << endl ;
        bool bstat ;
        bstat = config.Load( cfgpath ) ;
        if (!bstat) {
            cerr << "Unable to load configuration" << endl ;
            return false ;
        } else {
            if (Verbose) cout << "Loaded configuration" << endl ;
            config.Set(_overrides) ;            
        }
    } else {
        if (Verbose) cout << "No config file exists" << endl ;
        config.Set(_overrides) ;  
    }
    return true ;
}

bool Clone::Transform() {
    if (Verbose) cout << "Target dir " << destination << endl ;
    for(auto& p: fs::recursive_directory_iterator(destination)) {
        if (p.is_regular_file()) {
            bool bstat ;
            bstat = Transform(p.path().c_str());
            if (!bstat) {
                std::cerr << "Transform failed" << std::endl ;
                return false ;
            }
        } 
    } 
    return true ;
}


bool Clone::Transform(std::string _filename) {

    bool changed = false ;
    bool rename = false ;

    fs::path p(_filename) ;
    fs::path op = _filename + ".out" ;
    std::string newfilename ;
    if (Verbose) cout << "Transforming file " << _filename << endl ;
    if (Verbose) cout << "Directory " << p.parent_path() << endl ;
    if (Verbose) cout << "Temporary file " << op.c_str() << endl ;
    std::ofstream of ( op ) ;
    if (!of.is_open()) {
        cerr << "Unable to open temporary file " << op.c_str() << endl ;
        return false ;
    }
    std::ifstream inf (_filename);
    if (!inf.is_open()) {
        cerr << "Unable to open input file " << _filename << endl ;
        return false ;
    }
    while (!inf.eof()) {

        std::string inpline ;
        std::getline(inf,inpline) ;
       
        if (inpline.find( directive ) != string::npos ) {
            changed = true ;  

            std::string xfmline ;
            xfmline = Expand(inpline) ;  
            TransformStatus status ;
            std::string oline ;
            status = TransformLine(xfmline,oline) ;
            switch (status) {                
                case SUCCESS:
                    of << oline << endl ;                 
                    break ;
                case NOCHANGE:
                    of << xfmline << endl ;     
                    break ;
                case FILENAMECHANGE:
                    if (Verbose) cout << "File Name change to " << oline << endl ;
                    of << xfmline << endl ;
                    newfilename = oline ;
                    rename = true ;
                    break ;
                case UNDEFINED_MACRO:
                default:
                    cerr << "Status " << status << " processing line " << xfmline << endl ;
                    break ;
            }
        } else {
            of << inpline << endl ;
            continue ;
        }
    }
    of.close() ;
    inf.close() ;

    if (!changed) {
        fs::remove( op );
        return true ;
    }
    if (rename) {
        fs::remove( p ) ;
        fs::rename(op,newfilename) ;
    } else {
        fs::remove( p ) ;
        fs::rename(op,p) ;
    }
    return true ;
}

std::string Clone::Expand(std::string _line) {
    if (Verbose) cout << "Expanding: " << _line << endl ;
    std::smatch macros;
    while (std::regex_search (_line,macros,usemacro)) {
        
        if (Verbose) { 
            for (auto x:macros) std::cout << x << " : ";
                        std::cout << std::endl;
        }

        std::string repl ;
        repl = config.Get(macros[1]) ;
        if (Verbose) cout << "Replacing " << macros[0] << " with " << repl << endl ;
        std::string old=macros[0] ;
        boost::replace_all(_line, old , repl );
        if (Verbose) cout << "Final string " << _line << endl ;
    }

    return _line ;
}

TransformStatus Clone::TransformLine(std::string _line, std::string &_oline) {
    std::smatch macros;
    if (std::regex_search (_line,macros,renamecmd)) {
        if (Verbose) cout << "File name change directive " << endl ;
        _oline=macros[1] ;
        return FILENAMECHANGE ;

    }

    if (std::regex_search (_line,macros,subscmd)) {
        if (Verbose) cout << "Substitute directive " << endl ;
        std::string old = macros[1] ;
        std::string newstr = macros[2] ;
        if (Verbose) cout << "Replacing " << old << " with " << newstr << endl ;
        size_t dirpos = _line.find(directive) ;
        if (dirpos == string::npos) {
            return NOCHANGE ;
        }
        std::string oline = _line;
        size_t spos = oline.find(old) ;

        for ( ;; spos=oline.find(old)) {
            if (spos == string::npos) break ;
            if (spos >= oline.find(directive)) break ;
            oline = oline.replace( spos, old.length() , newstr ) ;
            cout << oline << endl ;
        }

        _oline = oline ;
        return SUCCESS ;
    }
    if (Verbose) cout << "No directives found in line " << _line << endl ;
    return NOCHANGE ;
}