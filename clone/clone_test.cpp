#include <string>
#include <iostream>
#include <vector>
#include "clone.hpp"

using namespace std ;
std::string repo("git@gitlab.com:cpp8/cli.git") ;
std::string dest("./clonetest") ;
int main1(int argc, char** argv) {

    if (argc > 1) {
        repo = argv[1] ;
        if (argc > 2) {
            dest = argv[2] ;
        }
    }
    std::vector< std::string > macros ;
    if (argc > 3) {
        for (int ptr=3; ptr<argc; ptr++) {
            macros.push_back( argv[ptr] );
        }
    }
    bool bstat ;
    Clone clone ;
    bstat = clone.Download(repo,dest) ;
    if (!bstat) {
        cerr << "Download failed. Quitting..." << endl ;
        exit(EXIT_FAILURE) ;
    }
    bstat = clone.Configure(macros) ;
    if (!bstat) {
        cerr << "Configure failed. Quitting..." << endl ;
        exit(EXIT_FAILURE) ;
    }
    bstat = clone.Transform() ;
    if (!bstat) {
        cerr << "Transform failed. Quitting..." << endl ;
        exit(EXIT_FAILURE) ;
    }
    exit(EXIT_SUCCESS) ;
}

int main2(int argc, char **argv) {
    std::vector< std::string > macros ;
    if (argc > 1) {
        for (int ptr=1; ptr<argc; ptr++) {
            macros.push_back( argv[ptr] );
        }
    }
    Clone clone ;
    clone.Configure(macros) ;
    clone.Expand("// proj: rename $(Proj)_main.cpp") ;
    clone.Expand("-- proj: subs __name__ $(Name)") ;
}

int main(int argc, char **argv) {
    std::vector< std::string > macros ;
    if (argc > 1) {
        for (int ptr=1; ptr<argc; ptr++) {
            macros.push_back( argv[ptr] );
        }
    }
    Clone clone ;
    clone.Configure(macros) ;
    TransformStatusType tstatus ;
    std::string renameline = clone.Expand("// proj: rename $(Proj)_main.cpp") ;
    std::string newfilename ;
    tstatus = clone.TransformLine(renameline, newfilename);

    if (tstatus == FILENAMECHANGE)
        cout << "File Rename " << renameline << endl << "New name " << newfilename << endl ;
    else cout << "Rename command failure" << endl ;

    std::string cline = clone.Expand("-- Author and Copyright (c) __name__ proj: subs __name__ $(Name)") ;


    std::string ctline ;
    tstatus = clone.TransformLine(cline, ctline);
    if (tstatus == SUCCESS)
        cout << "Transform line " << cline << endl << "Transformed " << ctline << endl ;
    else cout << "Failure in basic transform" << endl ;
}